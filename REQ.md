Create an in-car application that "calls" someone from your list of contacts.
The list of contacts is provided as `contacts.json`.

Here's how the application would work:
1. Driver sees screen with a "Place call" button.
2. Clicking that button will show an overlay with a list of contacts (from contacts.json)
3. The list of contacts is sortable, searchable and paginated (10 items per page max).
4. User scrolls, searches etc and finally selects an entry.
5. Modal closes and the Call Screen is shown " think Android or iPhone etc call screen but with some extra things.
6. Call Screen displays information about the selected user:
	- name
	- country and city
	- telephone number
	- days until next birthday or "Don't forget to wish you friend a happy birthday today". The birthdate is a UNIX timestamp (seconds).
	- small map of the last recorded position. This is available as `currentCoordinates` in the contacts.json file and represents a pair of (latitude, longitude) coordinates. Coordinates will be all over the place, but you tend to have many friends when you have a cool car.

Optional: Use a Front-End build system to build make your app ready for production.

