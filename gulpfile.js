"use strict";

var gulp = require('gulp');
var del = require('del');

var less = require('gulp-less');
var sourceMap = require('gulp-sourcemaps');

var connect = require('connect');
var connectLr = require('connect-livereload');
var connectStatic = require('serve-static');
var tinyLr = require('tiny-lr');
var path = require('path');

gulp.task('clean', function(cb) {
	del(['out/'], cb);
});

gulp.task('copy-html', function() {
	return copyHtml();
});

gulp.task('copy-res', function() {
	return copyRes();
});

gulp.task('copy-js', function() {
	return copyJs();
});

gulp.task('less', function() {
	return compileLess(false);
});

gulp.task('less:debug', function() {
	return compileLess(true);
});

gulp.task('build', ['copy-js', 'copy-html', 'copy-res', 'less']);

gulp.task('build:debug', ['copy-js', 'copy-html', 'copy-res', 'less:debug']);

gulp.task('watch', [], function() {
	gulp.watch('src/script/**/*.js', ['copy-js']);
	gulp.watch('src/style/**/*.less', ['less:debug']);
	gulp.watch('src/**/*.html', ['copy-html']);
});

gulp.task('livereload', ['watch'], function() {
	initLiveReload();
});

gulp.task('server', ['livereload'], function() {
	initServer();
});

gulp.task('default', ['build:debug', 'livereload', 'server']);

function copyHtml() {
	return copy('src/**/*.html', './src');
}

function copyRes() {
	return copy('res/**/*', './');
}

function copyJs() {
	return copy('src/script/**/*.js', 'src/');
}

function copy(what, baseDir) {
	return gulp.src(what, {base: baseDir}).pipe(gulp.dest('out/'));
}

function compileLess(debug) {
	var stream = gulp.src('src/style/app.less');
	if (debug) {
		stream = stream.pipe(sourceMap.init());
	}

	stream = stream.pipe(less({
		strictMath: true,
		strictUnits: true,
		strictImports: true,
		compress: !debug,
		cleanCss: !debug
	}));

	if (debug) {
		stream = stream.pipe(sourceMap.write());
	}

	return stream.pipe(gulp.dest('out/'));
}

function initLiveReload() {
	var lr = tinyLr();
	lr.listen(35279);
	gulp.watch('out/**/*', function(ev) {
		var file = path.relative('out/', ev.path);
		lr.changed({body: {files: [file]}});
	});
}

function initServer() {
	var srv = connect();
	srv
		.use(connectLr({port: 35279}))
		.use(connectStatic('out/'))
		.listen(8080);
}
