var app = angular.module('carDialer', [
	'ngRoute',
	'carDialerControllers',
	'carDialerFilters'
]);

app.config(function($routeProvider) {
	$routeProvider
		.when('/agenda', {
			templateUrl: 'partials/agenda.html',
			controller: 'AgendaController',
		})
		.when('/call/:id', {
			templateUrl: 'partials/call.html',
			controller: 'CallController'
		})
		.otherwise({
			redirectTo: '/'
		});
});
