var controllers = angular.module('carDialerControllers', ['carDialerServices']);

controllers.controller('AgendaController', function($scope, ContactService) {
	var contacts;
	var pageSize = 10;
	$scope.pageSize = pageSize;

	$scope.filteredContacts = null;
	$scope.currentPage = 1;
	$scope.sortBy = 'name';

	ContactService.getContacts(function(data) {
		contacts = data;
		sort($scope.sortBy);
	});

	function renderCurrentPage() {
		if (!$scope.filteredContacts) { return; }

		// short-circuit page 0 (happens when filtering) so that we don't .slice an empty array
		if ($scope.currentPage === 0) {
			$scope.contacts = null;
			return;
		}

		$scope.contacts = $scope.filteredContacts.slice(
			($scope.currentPage - 1) * pageSize, $scope.currentPage * pageSize
		);
	}

	function updateFiltered() {
		if (!$scope.filteredContacts) { return; }

		$scope.pages = Math.ceil($scope.filteredContacts.length / pageSize);

		if ($scope.pages >= 1 && $scope.currentPage !== 1) {
			$scope.currentPage = 1;
		} else if ($scope.pages === 0) {
			$scope.currentPage = 0;
		} else {
			// already on 1st page so re-render with updated contacts
			renderCurrentPage();
		}
	}

	function filter(query) {
		if (!contacts) { return; }

		// short-circuit empty-query as the entire array
		var filtered = contacts;

		if (query) {
			// search for query in all the rendered fields of a contact at the same time using a regexp
			var regex = new RegExp(query, 'i');
			filtered = contacts.filter(function(contact) {
				return regex.test(contact.firstName + ' ' + contact.lastName + ' ' + contact.phoneNumber);
			});
		}

		$scope.filteredContacts = filtered;
	}

	function nameComparator(first, second) {
		if (first.firstName < second.firstName) {
			return -1;
		} else if (first.firstName > second.firstName) {
			return 1;
		} else if (first.lastName < second.lastName) {
			return -1;
		} else if (first.lastName > second.lastName) {
			return 1;
		} else {
			return 0;
		}
	}

	function phoneComparator(first, second) {
		return first.phoneNumber < second.phoneNumber ? -1 : first.phoneNumber > second.phoneNumber ? 1 : 0;
	}

	var comparators = {
		phoneNumber: phoneComparator,
		name: nameComparator
	};

	function sort(field) {
		if (!contacts) { return; }

		// do a shallow copy because angular doesn't properly dirty-check moves in an array
		contacts = contacts.slice().sort(comparators[field]);

		// re-apply filtering to mantain sort order
		filter($scope.query);
	}

	$scope.$watch('currentPage', renderCurrentPage);
	$scope.$watch('filteredContacts', updateFiltered);
	$scope.$watch('query', filter);
	$scope.$watch('sortBy', sort);
});

controllers.controller('CallController', function($scope, $routeParams, ContactService) {
	ContactService.getContacts(function(data) {
		var id = parseInt($routeParams.id, 10);
		var contact;
		for (var i = 0, len = data.length; i < len; ++i) {
			if (data[i].id === id) {
				contact = data[i];
				break;
			}
		}

		$scope.contact = contact;
		$scope.callStatus = 'Dialing';

		var coords = contact.currentCoordinates.split(', ');
		renderMap.apply(null, coords.map(parseFloat));
	});

	function renderMap(lat, lng) {
		var map = L.map('map', {
			center: [lat, lng],
			zoom: 13,
			zoomControl: false,
			attributionControl: false
		});

		L.tileLayer('http://{s}.tiles.mapbox.com/v3/alexghr.jeih91ap/{z}/{x}/{y}.png', {
			maxZoom: 18
		}).addTo(map);

		L.marker([lat, lng]).addTo(map);
	}
});
