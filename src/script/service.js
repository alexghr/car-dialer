var services = angular.module('carDialerServices', ['ngResource']);

services.factory('ContactService', function($http, $resource) {
	return $resource('res/contacts.json', null, {getContacts: {isArray: true, responseType: 'json'}});
});

