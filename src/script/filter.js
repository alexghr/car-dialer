var filters = angular.module('carDialerFilters', []);

filters.filter('nextDate', function() {
	/**
	 * Computes the date on the next year if the date already passed,
	 * otherwise returns the same date
	 */
	return function(date) {
		var now = new Date();
		var next = new Date(date);

		next.setFullYear(now.getFullYear());
		if (next < now) {
			next.setFullYear(next.getFullYear() + 1);
		}

		return next;
	}
});

filters.filter('unixToDate', function() {
	/**
	 * Converts a UNIX timestamp (in seconds) to a Date object
	 */
	return function(input) {
		var seconds = +input;
		return new Date(seconds * 1000);
	};
});

filters.filter('daysLeft', function() {
	return function(input) {
		var dayInMs = 1000 * 60 * 60 * 24; 
		// use |0 to cast to int
		return ((input.getTime() - Date.now()) / dayInMs) | 0;
	};
});

